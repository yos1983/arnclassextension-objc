//
//  NSDate+ARN.h
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (ARN)

+ (NSDateFormatter *)arn_defaultFormatter;

// -------------------------------------------------------------------------------------------------------------------------------//
// Date ← String

+ (NSDate *)arn_dateForFormatString:(NSString *)formatString dateString:(NSString *)dateString;

+ (NSDate *)arn_formatForDateStringYYYY_MM_DD:(NSString *)dateString;

+ (NSDate *)arn_formatForDateStringYYYYMMDD_HHMMSS:(NSString *)dateString;

+ (NSDate *)arn_formatForDateStringYYYYMMDD:(NSString *)dateString;

// -------------------------------------------------------------------------------------------------------------------------------//
// String ← Date

- (NSString *)arn_stringWithFormatString:(NSString *)formatString;

- (NSString *)arn_stringWithFormatYYYYMMDD_HHMMSS;

- (NSString *)arn_stringWithFormatYYYYMMDD;

- (NSString *)arn_stringWithFormatYYYYMM;

- (NSString *)arn_stringWithFormatMDHM;

- (NSString *)arn_stringWithFormatHMM;

- (NSString *)arn_stringWithFormatD_Weakday;

- (NSString *)arn_stringWithFormatJapaneseYYYYMD;

- (NSString *)rss_stringWithFormatJapaneseYYYYM;

- (NSString *)arn_stringWithFormatJapaneseYYYYMD_Weakday;

- (NSString *)arn_stringWithFormatJapaneseMD_HM;

- (NSString *)arn_stringWithFormatJapaneseYYYYMD_HM;

- (NSString *)arn_displayDate;

// -------------------------------------------------------------------------------------------------------------------------------//
// Date ← Date

- (NSInteger)arn_ageForDate;

// -------------------------------------------------------------------------------------------------------------------------------//
// Other

+ (NSString *)arn_japaneseTimeHMMAtMinute:(NSInteger)minute;

- (NSDate *)arn_yesterday;

- (NSDate *)arn_tomorrow;

- (BOOL)arn_isEqualToMonth:(NSDate *)date;

- (BOOL)arn_isEqualToDay:(NSDate *)date;

@end
