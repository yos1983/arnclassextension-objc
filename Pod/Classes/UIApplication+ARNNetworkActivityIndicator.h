//
//  UIApplication+ARNNetworkActivityIndicator.h
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (ARNNetworkActivityIndicator)

/**
 *  ネットワーク中にnetworkActivityIndicatorを出す際の参照カウント的なものを内部に持ってて
 *  0:非表示　1以上:表示　させる
 *　ARNNetworkingの各マネージャは内部で行っているので不要
 */

/**
 *  networkActivityIndicatorのカウントアップ
 */
+ (void)arn_pushNetworkActivityIndicator;

/**
 *  networkActivityIndicatorのカウントダウン
 */
+ (void)arn_popNetworkActivityIndicator;

/**
 *  networkActivityIndicatorのカウントリセット
 */
+ (void)arn_resetNetworkActivityIndicator;

/**
 *  現在のカウント数を返す
 *
 *  @return カウント数
 */
+ (NSInteger)arn_requestCount;
@end
