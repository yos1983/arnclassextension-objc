//
//  UIView+ARNMotionEffect.h
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ARNMotionEffect)

- (void)arn_addMotionEffectAtLevel:(CGFloat)level NS_AVAILABLE_IOS(7_0);

@end
