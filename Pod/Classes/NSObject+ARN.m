//
//  NSObject+ARN.m
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#if !__has_feature(objc_arc)
#error This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#import "NSObject+ARN.h"

BOOL ARNEqualObjects(id obj1, id obj2) {
    return (obj1 == obj2 || [obj1 isEqual:obj2]);
}

@implementation NSObject (ARN)

@end
