//
//  UIImage+ARN.h
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ARN)

+ (UIImage *)arn_imageWithColor:(UIColor *)color;

// 範囲内にリサイズ
- (UIImage *)arn_resizeAspectFitWithinMaxWidth:(CGFloat)maxWidth
                                     maxHeight:(CGFloat)maxHeight
                                      minWidth:(CGFloat)minWidth
                                     minHeight:(CGFloat)minHeight;

// 拡大
- (UIImage *)arn_expansionAspectFitWithMaxWidth:(CGFloat)maxWidth
                                      maxHeight:(CGFloat)maxHeight;

// 縮小
- (UIImage *)arn_reducingAspectFitWithMinWidth:(CGFloat)minWidth
                                     minHeight:(CGFloat)minHeight;

- (NSData *)arn_jpegData;

@end
