//
//  NSString+ARN.m
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#if !__has_feature(objc_arc)
#error This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#import "NSString+ARN.h"

#import <CommonCrypto/CommonDigest.h> // for CC_MD5
#import <CommonCrypto/CommonCryptor.h> // for CCCryptor

@implementation NSString (ARN)

+ (NSString *)arn_uuidString
{
    return [[NSUUID UUID] UUIDString];
}

/*!
 指定のエンコードに変換する
 */
- (NSString *)arn_stringByUsingEncoding:(NSStringEncoding)encoding allowLossy:(BOOL)allowLossy
{
    NSData *data = [self dataUsingEncoding:encoding allowLossyConversion:allowLossy];
    if (!data) {
        return nil;
    }
    
    return [[NSString alloc] initWithData:data encoding:encoding];
}

/*!
 通常の stringByAddingPercentEscapesUsingEncoding はエンコードに失敗したらエラーだが、allowLossyを付けた
 */
- (NSString *)arn_stringByAddingPercentEscapesStrictUsingEncoding:(NSStringEncoding)encoding allowLossy:(BOOL)allowLossy
{
    /* 標準の"stringByAddingPercentEscapesUsingEncoding"は取りこぼしが多数あるので、自前で処理する
     * http://blog.daisukeyamashita.com/post/1686.html
     * http://blog.justoneplanet.info/2011/09/24/objective-c%E3%81%A7url%E3%82%A8%E3%83%B3%E3%82%B3%E3%83%BC%E3%83%89%E3%81%99%E3%82%8B/
     * などを参照
     * RFC3986
     * http://tools.ietf.org/html/rfc3986#section-2.1
     */
    CFStringRef strRef = CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                 // 指定のエンコードで変換
                                                                 (CFStringRef)[self arn_stringByUsingEncoding: encoding allowLossy: allowLossy],
                                                                 NULL,
                                                                 (CFStringRef) @"-._~:/?#[]@!$&'()*+,;=",
                                                                 CFStringConvertNSStringEncodingToEncoding(encoding));
    NSString *str = [NSString stringWithString:(__bridge_transfer NSString *) strRef];
    
    return str;
}

// -----------------------------------------------------------------------------------------------------------------------//
#pragma mark - Class Methods

+ (NSString *)arn_base64EncodeWithData:(NSData *)data
{
    static char table [] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSInteger      length = [data length];
    NSMutableData *mData  = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t       *input  = (uint8_t *) [data bytes];
    uint8_t       *output = (uint8_t *) mData.mutableBytes;
    
    for (NSInteger i = 0; i < length; i += 3) {
        NSInteger value = 0;
        for (NSInteger j = i; j < (i + 3); ++j) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xff & input[j]);
            }
        }
        
        NSInteger index = (i / 3) * 4;
        output[index + 0] =                    table[(value >> 18) & 0x3f];
        output[index + 1] =                    table[(value >> 12) & 0x3f];
        output[index + 2] = (i + 1) < length ? table[(value >> 6) & 0x3f] : '=';
        output[index + 3] = (i + 2) < length ? table[(value >> 0) & 0x3f] : '=';
    }
    
    return [[NSString alloc] initWithData:mData encoding:NSASCIIStringEncoding];
}

// 正規表現チェック
+ (BOOL)arn_checkRegex:(NSInteger)stringType string:(NSString *)checkString
{
    BOOL resultRegex = YES;
    
    NSError             *error = nil;
    NSRegularExpression *regexp;
    
    if (stringType == 1) {
        // パスワードチェックの場合(半角英数以外をチェック)
        regexp = [NSRegularExpression regularExpressionWithPattern:@"[^a-zA-Z0-9]"
                                                           options:0
                                                             error:&error];
    } else if (stringType == 2) {
        // ニックネームの場合(特殊文字や記号をチェック)
        regexp = [NSRegularExpression regularExpressionWithPattern:@"[/=~^¥|@`[{;+:*]},<.>/_／＝〜＾￥｜＠｀「『；＋：＊」』、＜。＞・＿]"
                                                           options:0
                                                             error:&error];
    }
    
    if (error == nil) {
        NSTextCheckingResult *match =
        [regexp firstMatchInString:checkString options:0 range:NSMakeRange(0, checkString.length)];
        // match.numberOfRangesが0以上の場合、指定文字以外の文字列が含まれている
        if (match.numberOfRanges > 0) {
            resultRegex = NO;
        }
    }
    
    return resultRegex;
}

// データ→文字列
+ (NSString *)arn_data2str:(NSData *)data
{
    const void      *bytes  = [data bytes];
    NSMutableString *strbuf = [NSMutableString new];
    for (int i = 0; i < [data length]; i++) {
        [strbuf appendFormat:@"%02x", *(((unsigned char *) bytes) + i)];
    }
    
    return strbuf;
}

// MD5ハッシュ値生成
+ (NSString *)arn_md5String:(NSString *)base
{
    const char   *cStr = [base UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result); // This is the md5 call
    NSData *myData = [NSData dataWithBytes:(const void *) result length:16];
    
    return [NSString arn_data2str:myData];
}

// SHA1ハッシュ値生成
+ (NSString *)arn_sha1String:(NSString *)base
{
    const char   *cStr = [base UTF8String];
    unsigned char result[20];
    CC_SHA1(cStr, (CC_LONG)strlen(cStr), result);
    NSData *myData = [NSData dataWithBytes:(const void *) result length:20];
    
    return [NSString arn_data2str:myData];
}

// 　暗号化による照合キー生成
+ (NSString *)arn_encodeString:(NSString *)base key:(NSString *)key
{
    const char *cStr = [base UTF8String];
    //    const NSString *key = DES_KEY_1;
    
    char keyPtr[kCCKeySizeDES + 1];
    bzero(keyPtr, sizeof(keyPtr));
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    size_t        dataSize      = [base length];
    size_t        bufferPtrSize = (dataSize + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    unsigned char bufferPtr[bufferPtrSize];
    memset((void *) bufferPtr, 0, bufferPtrSize);
    size_t movedBytes = 0;
    
    CCCryptorStatus ccStatus = CCCrypt(kCCEncrypt, kCCAlgorithmDES,
                                       kCCOptionECBMode | kCCOptionPKCS7Padding,
                                       keyPtr, kCCBlockSizeDES,
                                       NULL,
                                       cStr, dataSize,
                                       (void *) bufferPtr, bufferPtrSize, &movedBytes);
    if (ccStatus == kCCSuccess) {
        NSData *myData = [NSData dataWithBytes:(const void *) bufferPtr length:(NSUInteger) movedBytes];
        
        return [NSString arn_data2str:myData];
    }
    
    return nil;
}

// ユーザーエージェント取得
+ (NSString *)arn_userAgent
{
    // TODO:共通ライブラリのバージョンを取得できるようにする
    return [NSString stringWithFormat:@"(%@/%@; %@; iOS/%@)",
            [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"],  // アプリ名
            [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"],  // アプリバージョン
            [UIDevice currentDevice].model,  // 端末モデル
            [UIDevice currentDevice].systemVersion];  // 端末OSバージョン
}

// 半角英数字のみか判定
+ (BOOL)arn_isHalfAlfabetOrNumberWithString:(NSString *)string
{
    NSMutableCharacterSet *checkCharSet = [[NSMutableCharacterSet alloc] init];
    [checkCharSet addCharactersInString:@"abcdefghijklmnopqrstuvwxyz"];
    [checkCharSet addCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
    [checkCharSet addCharactersInString:@"1234567890"];
    
    if ([[string stringByTrimmingCharactersInSet:checkCharSet] length] > 0) {
        return NO;
    }
    
    return YES;
}

// ひらがなが含まれているか判定
+ (BOOL)arn_isHiraganaWithString:(NSString *)string
{
    NSString             *pattern = @"^[ぁ-ん]+$";
    NSRegularExpression  *regex   = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
    NSTextCheckingResult *match   = [regex firstMatchInString:string options:0 range:NSMakeRange(0, string.length)];
    
    return match.numberOfRanges > 0;
}

// カタカナが含まれているか判定
+ (BOOL)arn_isKatakanaWithString:(NSString *)string
{
    NSString             *pattern = @"^[ァ-ン]+$";
    NSRegularExpression  *regex   = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
    NSTextCheckingResult *match   = [regex firstMatchInString:string options:0 range:NSMakeRange(0, string.length)];
    
    return match.numberOfRanges > 0;
}

// カタカナをひらがなへ変換
+ (NSString *)arn_stringKatakanaToHiraganaWithString:(NSString *)string
{
    return [self arn_stringTransformWithTransform:kCFStringTransformHiraganaKatakana
                                      reverse:true
                                       string:string];
}

// ひらがなをカタカナへ変換
+ (NSString *)arn_stringHiraganaToKatakanaWithString:(NSString *)string
{
    NSMutableString *hstr = [NSMutableString stringWithString:string];
    CFStringTransform((__bridge CFMutableStringRef) hstr, NULL, kCFStringTransformFullwidthHalfwidth, true);
    
    return [self arn_stringTransformWithTransform:kCFStringTransformHiraganaKatakana
                                      reverse:false
                                       string:hstr];
}

// -----------------------------------------------------------------------------------------------------------------------//
#pragma mark - Private Methods

+ (NSString *)arn_stringTransformWithTransform:(CFStringRef)transform reverse:(Boolean)reverse string:(NSString *)string
{
    NSMutableString *retStr = [NSMutableString stringWithString:string];
    CFStringTransform((__bridge CFMutableStringRef) retStr, NULL, transform, reverse);
    
    return retStr;
}

@end
