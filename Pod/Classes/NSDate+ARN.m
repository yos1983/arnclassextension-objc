//
//  NSDate+ARN.m
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#if !__has_feature(objc_arc)
#error This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#import "NSDate+ARN.h"

@implementation NSDate (ARN)

+ (NSDateFormatter *)arn_defaultFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setCalendar:[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar]];
    
    return formatter;
}

+ (NSArray *)arn_weekSymbol
{
    return @[@"日", @"月", @"火", @"水", @"木", @"金", @"土"];
}

// -------------------------------------------------------------------------------------------------------------------------------//
#pragma mark - Date ← String

+ (NSDate *)arn_dateForFormatString:(NSString *)formatString dateString:(NSString *)dateString
{
    NSDateFormatter *formatter = [self arn_defaultFormatter];
    [formatter setDateFormat:formatString];
    NSDate *date = [formatter dateFromString:dateString];
    
    return date;
}

+ (NSDate *)arn_formatForDateStringYYYY_MM_DD:(NSString *)dateString
{
    return [[self class] arn_dateForFormatString:@"yyyy-MM-dd" dateString:dateString];
}

+ (NSDate *)arn_formatForDateStringYYYYMMDD_HHMMSS:(NSString *)dateString
{
    return [[self class] arn_dateForFormatString:@"yyyy-MM-dd HH:mm:ss" dateString:dateString];
}

+ (NSDate *)arn_formatForDateStringYYYYMMDD:(NSString *)dateString
{
    return [[self class] arn_dateForFormatString:@"yyyy/MM/dd" dateString:dateString];;
}

// -------------------------------------------------------------------------------------------------------------------------------//
#pragma mark - String ← Date

- (NSString *)arn_stringWithFormatString:(NSString *)formatString
{
    NSDateFormatter *formatter = [[self class] arn_defaultFormatter];
    [formatter setDateFormat:formatString];
    NSString *dateString = [formatter stringFromDate:self];
    return dateString;
}

- (NSString *)arn_stringWithFormatYYYYMMDD_HHMMSS
{
    return [self arn_stringWithFormatString:@"yyyy-MM-dd HH:mm:ss"];
}

- (NSString *)arn_stringWithFormatYYYYMMDD
{
    return [self arn_stringWithFormatString:@"yyyy/MM/dd"];
}

- (NSString *)arn_stringWithFormatYYYYMM
{
    return [self arn_stringWithFormatString:@"yyyy-MM"];
}

- (NSString *)arn_stringWithFormatMDHM
{
    return [self arn_stringWithFormatString:@"M/d H:m"];
}

- (NSString *)arn_stringWithFormatHMM
{
    NSCalendar       *cal        = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [cal components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:self];
    
    return [NSString stringWithFormat:@"%2d:%02d", (unsigned int)components.hour, (unsigned int)components.minute];
}

- (NSString *)arn_stringWithFormatD_Weakday
{
    NSCalendar       *cal        = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:self];
    NSDate           *date       = [cal dateFromComponents:components];
    
    NSArray *weakdaysArray = [[self class] arn_weekSymbol];
    
    NSDateFormatter *formatter = [[self class] arn_defaultFormatter];
    // NSDateComponentsのweekdayの値は1〜7(日〜土)だから1引く
    [formatter setDateFormat:[NSString stringWithFormat:@"d %@", weakdaysArray[[components weekday] - 1]]];
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

- (NSString *)arn_stringWithFormatJapaneseYYYYMD
{
    return [self arn_stringWithFormatString:@"yyyy年M月d日"];
}

- (NSString *)rss_stringWithFormatJapaneseYYYYM
{
    NSCalendar       *cal        = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:self];
    NSDate           *date       = [cal dateFromComponents:components];
    
    NSDateFormatter *formatter = [[self class] arn_defaultFormatter];
    [formatter setDateFormat:@"yyyy年M月"];
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

- (NSString *)arn_stringWithFormatJapaneseYYYYMD_Weakday
{
    NSCalendar       *cal        = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:self];
    NSDate           *date       = [cal dateFromComponents:components];
    
    NSArray *weakdaysArray = [[self class] arn_weekSymbol];
    
    NSDateFormatter *formatter = [[self class] arn_defaultFormatter];
    // NSDateComponentsのweekdayの値は1〜7(日〜土)だから1引く
    [formatter setDateFormat:[NSString stringWithFormat:@"yyyy年M月d日 (%@)", weakdaysArray[[components weekday] - 1]]];
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

- (NSString *)arn_stringWithFormatJapaneseMD_HM
{
    return [self arn_stringWithFormatString:@"M月d日 H時m分"];
}

- (NSString *)arn_stringWithFormatJapaneseYYYYMD_HM
{
    return [self arn_stringWithFormatString:@"yyyy年M月d日 H時m分"];
}

- (NSString *)arn_displayDate
{
    NSDate        *now    = [NSDate date];
    NSTimeInterval passed = [now timeIntervalSinceDate:self];
    int            min    = passed / 60;
    if (min <= 1) {
        return @"1分前";
    }
    if (min < 60) {
        return [NSString stringWithFormat:@"%d分前", min];
    }
    int hour = min / 60;
    if (hour < 24) {
        return [NSString stringWithFormat:@"%d時間前", hour];
    }
    int day = hour / 24;
    return [NSString stringWithFormat:@"%d日前", day];
}

// -------------------------------------------------------------------------------------------------------------------------------//
#pragma mark - Date ← Date

- (NSInteger)arn_ageForDate
{
    NSCalendar       *cal  = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSCalendarUnit    unit = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *elap = [cal components:unit fromDate:self toDate:[NSDate date] options:0];
    
    return elap.year;
}

// -------------------------------------------------------------------------------------------------------------------------------//
#pragma mark - Other

+ (NSString *)arn_japaneseTimeHMMAtMinute:(NSInteger)minute
{
    NSInteger hour = minute / 60;
    NSInteger minu = minute % 60;
    if (!hour) {
        return [NSString stringWithFormat:@"%d分", (unsigned int)minute];
    } else {
        return [NSString stringWithFormat:@"%d時間%d分", (unsigned int)hour, (unsigned int)minu];
    }
}

- (NSDate *)arn_yesterday
{
    NSCalendar       *cal        = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];
    components.day = components.day - 1;
    return [cal dateFromComponents:components];
}

- (NSDate *)arn_tomorrow
{
    NSCalendar       *cal        = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];
    components.day = components.day + 1;
    return [cal dateFromComponents:components];
}

- (BOOL)arn_isEqualToMonth:(NSDate *)date
{
    NSCalendar       *cal            = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *selfComponents = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:self];
    NSDateComponents *components     = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:date];
    if (selfComponents.month == components.month) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)arn_isEqualToDay:(NSDate *)date
{
    NSCalendar       *cal            = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *selfComponents = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];
    NSDateComponents *components     = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    if (selfComponents.day == components.day) {
        return YES;
    } else {
        return NO;
    }
}

@end
