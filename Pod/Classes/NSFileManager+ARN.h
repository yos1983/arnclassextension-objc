//
//  NSFileManager+ARN.h
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (ARN)

/*!
 Documentsディレクトリへのパス
 実機の場合
 =>
 シミュレータの場合
 => /Users/admin/Library/Application Support/iPhone Simulator/6.1/Applications/C139EA44-C01B-45EB-8D97-B664BFEB8A3A/Documents
 */
+ (NSString *)arn_pathForDocuments;

/*!
 Cachesディレクトリへのパス
 実機の場合
 =>
 シミュレータの場合
 => /Users/admin/Library/Application Support/iPhone Simulator/6.1/Applications/C139EA44-C01B-45EB-8D97-B664BFEB8A3A/Library/Caches
 */
+ (NSString *)arn_pathForCaches;

- (BOOL)arn_createDirectoryAndFileAtPath:(NSString *)path contents:(NSData *)contents error:(NSError **)error;

@end
