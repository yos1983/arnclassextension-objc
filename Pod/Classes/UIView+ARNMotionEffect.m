//
//  UIView+ARNMotionEffect.m
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#if !__has_feature(objc_arc)
#error This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#import "UIView+ARNMotionEffect.h"

@implementation UIView (ARNMotionEffect)

- (void)arn_addMotionEffectAtLevel:(CGFloat)level
{
    UIInterpolatingMotionEffect *xAxis;
    xAxis = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x"
                                                            type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    xAxis.minimumRelativeValue = [NSNumber numberWithFloat:-10.0f * level];
    xAxis.maximumRelativeValue = [NSNumber numberWithFloat:10.0f * level];
    
    UIInterpolatingMotionEffect *yAxis;
    yAxis = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y"
                                                            type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    yAxis.minimumRelativeValue = [NSNumber numberWithFloat:-10.0f * level];
    yAxis.maximumRelativeValue = [NSNumber numberWithFloat:10.0f * level];
    
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[xAxis, yAxis];
    
    [self addMotionEffect:group];
}


@end
