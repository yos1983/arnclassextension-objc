//
//  NSFileManager+ARN.m
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#if !__has_feature(objc_arc)
#error This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#import "NSFileManager+ARN.h"

@implementation NSFileManager (ARN)

/*!
 Documentsディレクトリへのパス
 */
+ (NSString *)arn_pathForDocuments
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

/*!
 Cachesディレクトリへのパス
 */
+ (NSString *)arn_pathForCaches
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

/*!
 指定のファイルを作成する
 途中のディレクトリが無い場合、作成する
 */
- (BOOL)arn_createDirectoryAndFileAtPath:(NSString *)path contents:(NSData *)contents error:(NSError *__autoreleasing *)error
{
    /* 親ディレクトリのパス: 「"/tmp/image.png" => "/tmp"」, 「"/" => "/"」 */
    NSString *parentDirPath = [path stringByDeletingLastPathComponent];
    if ([parentDirPath isEqualToString:@""]) {
        return NO;  // パスが異常: 「"image.png" => ""」
    }
    /* ディレクトリの作成 */
    BOOL isDirectory;
    if (![self fileExistsAtPath:parentDirPath isDirectory:&isDirectory]) {
        // 存在しなければ作成 (XXX: ディレクトリではなく通常のファイルの場合は削除しても良いものか)
        if (![self createDirectoryAtPath:parentDirPath withIntermediateDirectories:YES attributes:nil error:error]) {
            // ディレクトリの作成に失敗。(ちなみに、既にディレクトリが存在する場合でもエラーにはならないので、ここには来ない)
            return NO;
        }
    }
    
    /* ファイルの作成 */
    return [self createFileAtPath:path contents:contents attributes:nil];
}

@end
