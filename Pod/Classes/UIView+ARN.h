//
//  UIView+ARN.h
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ARN)

+ (instancetype)arn_createViewByNib;

- (void)arn_closeKeyboad;

@end
