//
//  UIImage+ARN.m
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#if !__has_feature(objc_arc)
#error This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#import "UIImage+ARN.h"

@implementation UIImage (ARN)

+ (UIImage *)arn_imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(contextRef, [color CGColor]);
    CGContextFillRect(contextRef, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage *)arn_resizeAspectFitWithinMaxWidth:(CGFloat)maxWidth
                                     maxHeight:(CGFloat)maxHeight
                                      minWidth:(CGFloat)minWidth
                                     minHeight:(CGFloat)minHeight;
{
    CGSize size = self.size;
    if (size.width > maxWidth || size.height > maxHeight) {
        return [self arn_reducingAspectFitWithMinWidth:minWidth minHeight:minHeight];
    } else if (size.width < minWidth && size.height < minHeight) {
        return [self arn_expansionAspectFitWithMaxWidth:minWidth maxHeight:minHeight];
    }
    return self;
}

- (UIImage *)arn_expansionAspectFitWithMaxWidth:(CGFloat)maxWidth
                                      maxHeight:(CGFloat)maxHeight
{
    if (self.size.width <= maxWidth && self.size.height <= maxHeight) {
        return self;
    }
    
    CGFloat widthRatio = maxWidth / self.size.width;
    
    CGSize resizedSize = CGSizeMake(self.size.width * widthRatio, self.size.height * widthRatio);
    
    if (resizedSize.height > maxHeight) {
        CGFloat heightRatio = maxHeight / resizedSize.height;
        resizedSize = CGSizeMake(resizedSize.width * heightRatio, resizedSize.height * heightRatio);
    }
    
    UIGraphicsBeginImageContext(resizedSize);
    [self drawInRect:CGRectMake(0, 0, resizedSize.width, resizedSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resizedImage;
}

- (UIImage *)arn_reducingAspectFitWithMinWidth:(CGFloat)minWidth
                                     minHeight:(CGFloat)minHeight
{
    if (self.size.width <= minWidth && self.size.height <= minHeight) {
        return self;
    }
    
    CGFloat widthRatio = minWidth / self.size.width;
    
    CGSize resizedSize = CGSizeMake(self.size.width * widthRatio, self.size.height * widthRatio);
    
    if (resizedSize.height > minHeight) {
        CGFloat heightRatio = minHeight / resizedSize.height;
        resizedSize = CGSizeMake(resizedSize.width * heightRatio, resizedSize.height * heightRatio);
    }
    
    UIGraphicsBeginImageContext(resizedSize);
    [self drawInRect:CGRectMake(0, 0, resizedSize.width, resizedSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resizedImage;
}

- (NSData *)arn_jpegData
{
    NSData *imgData = UIImageJPEGRepresentation(self, 0.7f);
    return imgData;
}

@end