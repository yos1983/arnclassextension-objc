//
//  UIApplication+ARNNetworkActivityIndicator.m
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#if !__has_feature(objc_arc)
#error This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#import "UIApplication+ARNNetworkActivityIndicator.h"

static NSInteger arn_requestCount_ = 0;

@implementation UIApplication (ARNetworkActivityIndicator)

+ (void)arn_updateNetworkActivityIndicator
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = (0 < arn_requestCount_);
    });
}

+ (void)arn_pushNetworkActivityIndicator
{
    @synchronized(self) {
        ++arn_requestCount_;
    }
    [[self class] arn_updateNetworkActivityIndicator];
}

+ (void)arn_popNetworkActivityIndicator
{
    @synchronized(self) {
        --arn_requestCount_;
        if (arn_requestCount_ < 0) {
            arn_requestCount_ = 0;
        }
    }
    [[self class] arn_updateNetworkActivityIndicator];
}

+ (void)arn_resetNetworkActivityIndicator
{
    arn_requestCount_ = 0;
    [[self class] arn_updateNetworkActivityIndicator];
}

+ (NSInteger)arn_requestCount
{
    return arn_requestCount_;
}

@end
