//
//  NSObject+ARN.h
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#import <Foundation/Foundation.h>

BOOL ARNEqualObjects(id obj1, id obj2);


@interface NSObject (ARN)

@end
