//
//  UIView+ARN.m
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#if !__has_feature(objc_arc)
#error This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#import "UIView+ARN.h"

@implementation UIView (ARN)

+ (instancetype)arn_createViewByNib
{
    return [[[UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil] instantiateWithOwner:self options:nil] objectAtIndex:0];
}

- (void)arn_closeKeyboad
{
    [self endEditing:YES];
}

@end
