//
//  NSString+ARN.h
//  ARNClassExtension
//
//  Created by Airin on 2014/11/02.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ARN)

+ (NSString *)arn_uuidString;
/*!
 指定のエンコードに変換する
 */
- (NSString *)arn_stringByUsingEncoding:(NSStringEncoding)encoding allowLossy:(BOOL)allowLossy;

/*!
 通常の stringByAddingPercentEscapesUsingEncoding ではエンコード対象とならない @"$&+,/:;=?@"@"!*'()%#[]~" もエンコードする
 また、allowLossyでエラーにするかを選択できる
 */
- (NSString *)arn_stringByAddingPercentEscapesStrictUsingEncoding:(NSStringEncoding)encoding allowLossy:(BOOL)allowLossy;

+ (NSString *)arn_base64EncodeWithData:(NSData *)data;

// 正規表現チェック
+ (BOOL)arn_checkRegex:(NSInteger)stringType string:(NSString *)checkString;

// データ→文字列
+ (NSString *)arn_data2str:(NSData *)data;

// MD5ハッシュ値生成
+ (NSString *)arn_md5String:(NSString *)base;

// SHA1ハッシュ値生成
+ (NSString *)arn_sha1String:(NSString *)base;

// 　暗号化による照合キー生成
+ (NSString *)arn_encodeString:(NSString *)base key:(NSString *)key;

// ユーザーエージェント取得
+ (NSString *)arn_userAgent;

// 半角英数字のみか判定
+ (BOOL)arn_isHalfAlfabetOrNumberWithString:(NSString *)string;

// ひらがなが含まれているか判定
+ (BOOL)arn_isHiraganaWithString:(NSString *)string;

// カタカナが含まれているか判定
+ (BOOL)arn_isKatakanaWithString:(NSString *)string;

// カタカナをひらがなへ変換
+ (NSString *)arn_stringKatakanaToHiraganaWithString:(NSString *)string;

// ひらがなをカタカナへ変換
+ (NSString *)arn_stringHiraganaToKatakanaWithString:(NSString *)string;

@end
