# ARNClassExtension-objc

[![CI Status](http://img.shields.io/travis/xxxAIRINxxx/ARNClassExtension-objc.svg?style=flat)](https://travis-ci.org/xxxAIRINxxx/ARNClassExtension-objc)
[![Version](https://img.shields.io/cocoapods/v/ARNClassExtension-objc.svg?style=flat)](http://cocoadocs.org/docsets/ARNClassExtension-objc)
[![License](https://img.shields.io/cocoapods/l/ARNClassExtension-objc.svg?style=flat)](http://cocoadocs.org/docsets/ARNClassExtension-objc)
[![Platform](https://img.shields.io/cocoapods/p/ARNClassExtension-objc.svg?style=flat)](http://cocoadocs.org/docsets/ARNClassExtension-objc)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

* iOS 7.0+
* ARC

## Installation

ARNClassExtension-objc is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "ARNClassExtension-objc"

## License

ARNClassExtension-objc is available under the MIT license. See the LICENSE file for more info.

