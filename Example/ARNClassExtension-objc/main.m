//
//  main.m
//  ARNClassExtension-objc
//
//  Created by xxxAIRINxxx on 11/02/2014.
//  Copyright (c) 2014 xxxAIRINxxx. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ARNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ARNAppDelegate class]));
    }
}
